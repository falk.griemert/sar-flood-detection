import os
import sys

snappy_directory = "~/.snap/snap-python"
sys.path.append(snappy_directory)

import snappy
from snappy import Product
from snappy import ProductIO
from snappy import ProductUtils
#from snappy import WKTReader
from snappy import HashMap
from snappy import GPF
from snappy import jpy
# For shapefiles
import shapefile
import pygeoif

# Define Subset 
x = 10840
y = 4952
width = 18491
height = 11826


## Helper functions
def showProductInformation(product):
    width = product.getSceneRasterWidth()
    print("Width: {} px".format(width))
    height = product.getSceneRasterHeight()
    print("Height: {} px".format(height))
    name = product.getName()
    print("Name: {}".format(name))
    band_names = product.getBandNames()
    print("Band names: {}".format(", ".join(band_names)))
    

## Preprocessing functions
def applyOrbit(product):
    parameters = HashMap()
    parameters.put('orbitType', 'Sentinel Precise (Auto Download)')
    parameters.put('polyDegree', '3')
    parameters.put('continueOnFail', 'false')
    return GPF.createProduct('Apply-Orbit-File', parameters, product)

def subset(product):
  
    parameters = HashMap()
    SubsetOp = jpy.get_type('org.esa.snap.core.gpf.common.SubsetOp')
    geometry = WKTReader().read(wkt)
    parameters.put('copyMetadata', True)
    parameters.put('region', "%s,%s,%s,%s" % (x, y, width, height))
    return GPF.createProduct('Subset', parameters, product)

def calibration(product):
    parameters = HashMap()
    parameters.put('outputSigmaBand', True)
    parameters.put('sourceBands', 'Intensity_VV')
    parameters.put('selectedPolarisations', "VV")
    parameters.put('outputImageScaleInDb', False)
    return GPF.createProduct("Calibration", parameters, product)

def speckleFilter(product):
    parameters = HashMap()
    filterSizeY = '5'
    filterSizeX = '5'
    parameters.put('sourceBands', 'Sigma0_VV')
    parameters.put('filter', 'Lee')
    parameters.put('filterSizeX', filterSizeX)
    parameters.put('filterSizeY', filterSizeY)
    parameters.put('dampingFactor', '2')
    parameters.put('estimateENL', 'true')
    parameters.put('enl', '1.0')
    parameters.put('numLooksStr', '1')
    parameters.put('targetWindowSizeStr', '3x3')
    parameters.put('sigmaStr', '0.9')
    parameters.put('anSize', '50')
    return GPF.createProduct('Speckle-Filter', parameters, product)

def terrainCorrection(product):
    parameters = HashMap()
    parameters.put('demName', 'SRTM 3Sec')
    parameters.put('pixelSpacingInMeter', 10.0)
    parameters.put('sourceBands', 'Sigma0_VV')
    return GPF.createProduct("Terrain-Correction", parameters, product)


# Flooding processing
def generateBinaryFlood(product):
    parameters = HashMap()
    BandDescriptor =
    snappy.jpy.get_type('org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor')
    targetBand = BandDescriptor()
    targetBand.name = 'flooded'
    targetBand.type = 'uint8'
    targetBand.expression = '(Sigma0_VV < 1.13E-2) ? 1 : 0'
    targetBands =
    snappy.jpy.array('org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor', 1)
    targetBands[0] = targetBand
    parameters.put('targetBands', targetBands)
    return GPF.createProduct('BandMaths', parameters, product)


def maskKnownWater(product):
# Add land cover band
    parameters = HashMap()
    parameters.put("landCoverNames", "GlobCover")
    mask_with_land_cover = GPF.createProduct('AddLandCover', parameters,
    product)
    del parameters
    # Create binary water band
    BandDescriptor = snappy.jpy.get_type('org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor')
    parameters = HashMap()
    targetBand = BandDescriptor()
    targetBand.name = 'BinaryWater'
    targetBand.type = 'uint8'
    targetBand.expression = '(land_cover_GlobCover == 210) ? 0 : 1'
    targetBands = snappy.jpy.array('org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor', 1)
    targetBands[0] = targetBand
    parameters.put('targetBands', targetBands)

    water_mask = GPF.createProduct('BandMaths', parameters,
    mask_with_land_cover)
    del parameters
    parameters = HashMap()
    BandDescriptor = snappy.jpy.get_type('org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor')
    try:
        water_mask.addBand(product.getBand("flooded"))
    except:
        pass
    targetBand = BandDescriptor()
    targetBand.name = 'Sigma0_VV_Flood_Masked'
    targetBand.type = 'uint8'
    targetBand.expression = '(BinaryWater == 1 && flooded == 1) ? 1 : 0'
    targetBands = snappy.jpy.array('org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor', 1)
    targetBands[0] = targetBand
    parameters.put('targetBands', targetBands)
    return GPF.createProduct('BandMaths', parameters, water_mask)

if __name__ == "__main__":
    ## GPF Initialization
    GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis()
    ## Product initialization
    path_to_sentinel_data = "data/SENTINEL1.zip"
    product = ProductIO.readProduct(path_to_sentinel_data)
    showProductInformation(product)
    product_orbitfile = applyOrbit(product)
    product_subset = subset(product_orbitfile)
    showProductInformation(product_subset)
    # Apply remainder of processing steps in a nested function call
    product_preprocessed = terrainCorrection(
                            speckleFilter(
                                calibration(
                                    product_subset
                                )
                            )
                        )

    product_binaryflood = maskKnownWater(
                            generateBinaryFlood(
                                product_preprocessed
                            )
                        )
    ProductIO.writeProduct(product_binaryflood, "data/final_mask",'GeoTIFF')