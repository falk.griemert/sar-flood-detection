## Flood detection using S-1 SAR Data and snappy

This script utilizes ESA SNAP snappy to analyze SAR (Synthetic Aperture Radar) data retrieved by the ESA Sentinel-1 satellites.The global dataset can be accessed at https://scihub.copernicus.eu .

<u>Requirements:</u>
* Download ESA SNAP Toolbox (release v9.0) at  https://step.esa.int/main/download/snap-download/
* Configure snappy during and after the installation process

<hr>
This is an example of mapping  the Indus River floods in 2022. The image detail shows the region around Larkana (August 30).

![](flood_map.png)
*Flood overlay created from binary water band*

![](sattelite.png)
*Original landscape (Data source: Esri)*